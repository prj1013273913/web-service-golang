package model

import "myapp/dataStore/postgres"

type Admin struct {
	FirstName string 
	LastName  string
	Email     string
	Password  string
}

const queryInsertAdmin = "INSERT INTO admin (firstname, lastname, email, password) VALUES($1, $2, $3, $4) Returning email;"

func (adm *Admin) Create() error {
	row := postgres.Db.QueryRow(queryInsertAdmin, adm.FirstName, adm.LastName, adm.Email, adm.Password)
	err := row.Scan(&adm.Email)
	return err
}

// 1. SQL Query String:
//    ```sql
//    INSERT INTO admin (firstname, lastname, email, password) VALUES($1, $2, $3, $4) Returning email;
//    ```
//    - `INSERT INTO admin`: This part specifies that you're inserting data into the `admin` table.
//    - `(firstname, lastname, email, password)`: These are the columns into which data will be inserted.
//    - `VALUES($1, $2, $3, $4)`: This part corresponds to the values that will be inserted into the respective columns. `$1`, `$2`, `$3`, and `$4` are placeholders for the actual values.
//    - `Returning email`: This clause specifies that after the insert operation, you want to return the value of the `email` column for the newly inserted row.

// 2. Function `func (adm *Admin) Create() error`:
//    - This function is associated with the `Admin` struct and is intended to create a new admin record in the database.
//    - Inside the function, it executes the SQL query using `postgres.Db.QueryRow()`.
//    - `postgres.Db` likely represents the database connection.
//    - `QueryRow()` executes a query that is expected to return at most one row.
//    - The placeholders `$1`, `$2`, `$3`, and `$4` in the query string will be replaced by the values provided in the function call.
//    - `err := row.Scan(&adm.Email)`: This line scans the returned row from the query and extracts the value of the `email` column into the `Email` field of the `Admin` struct (`adm`).

// Confirmation of Successful Insertion: By returning the email, the code can verify that the insertion was successful. If an error occurs during the insertion process, the absence of a returned email would signal that the operation failed.
// Feedback to the Caller: The caller of the Create() function might need the email for further processing or to display a confirmation message to the user. Returning the email allows the caller to access this information directly without having to perform an additional query.
// Consistency and Integrity: Returning the email ensures that the caller receives the exact email that was inserted into the database. This helps maintain consistency and integrity in the data handling process.
// Error Handling: If there's a constraint violation or any other issue preventing the insertion of the record, the absence of a returned email can indicate to the caller that something went wrong during the operation.

const queryGetAdmin = "SELECT email, password FROM admin WHERE email=$1 and password=$2;"

func (adm *Admin) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, adm.Email, adm.Password).Scan(&adm.Email, &adm.Password)
}

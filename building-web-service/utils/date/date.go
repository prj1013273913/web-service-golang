package date

import "time"

// constants are usually declared like below when there are multiple constants in one code body

const (
	apiDateLayout = "2006-01-02T15:04:05Z"
)

// const apiDateLayout="2006-01-02T15:04:05Z"  if there is only one constant it is valid to
// decalre it like this.

func GetDate() string {
	date := time.Now().UTC()          //get current date and time
	return date.Format(apiDateLayout) //convert to string
}

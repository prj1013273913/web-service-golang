function signUp() {
    // data to be sent to the POST request
    console.log("hii")
    var _data = {
      firstname: document.getElementById("fname").value,
      lastname: document.getElementById("lname").value,
      email: document.getElementById("email").value,
      password: document.getElementById("pw1").value,
      pw: document.getElementById("pw2").value,
    };
    if (_data.password !== _data.pw) {
      alert("PASSWORD doesn't match!");
      return;
    }
    fetch("/signup", {
      method: "POST",
      body: JSON.stringify(_data),
      headers: { "Content-type": "application/json; charset=UTF-8" },
    }).then((response) => {
      /* The line `if (response.status == 201) {` is checking if the HTTP response status from the
      server is equal to 201. In HTTP, a status code of 201 indicates that a resource has been
      successfully created on the server. So, in this context, if the response status is 201, it
      means that the sign-up request was successful, and the user can be redirected to the login
      page. */
      if (response.status == 201) {
        // console.log("logged in")
        window.open("index.html", "_self");
      }
    });
  }
/* The code snippet is checking if the `document.cookie` is empty. If the cookie is empty, it means
that the user is not logged in. In that case, an alert message "User not logged in!!" is displayed
to the user, and the page is redirected to "index.html" using `window.open("index.html", "_self")`. */
if (document.cookie == "") {
  alert("User not logged in!!");
  window.open("index.html", "_self");
} else {
  console.log("cookie set");
}

/**
 * The `logout` function sends a request to the server to log the user out and redirects to the index
 * page if successful, otherwise it displays an error message.
 */
function logout() {
  fetch("/logout")
    .then((response) => {
      if (response.ok) {
        window.open("index.html", "_self");
      } else {
        throw new Error(response.statusText);
      }
    })
    .catch((e) => {
      alert(e);
    });
}

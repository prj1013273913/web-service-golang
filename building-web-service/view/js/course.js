window.onload= function(){
    fetch("/courses")
    .then((response)=>response.text())
    .then((data)=> showCourses(data));
};

function addCourse() {
  var data = getFormData();
  var cid = data.cid;
  if (isNaN(cid)) {
    alert("Enter a valid student ID");
    return;
  } else if (data.coursename == "") {
    alert("Enter a valid course name");
    return;
  }
  fetch("/course", {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "content-type": "application/json; charset=UTF-8" },
  })
    .then((response1) => {
      if (response1.ok) {
        fetch("/course/" + cid)
          .then((response2) => response2.text())
          .then((data) => showCourse(data));
      } else {
        throw new Error(response1.status);
      }
    })
    .catch((e) => {
      if (e.message == 303) {
        alert("User not logged in.");
        window.open("index.html", "_self");
      } else if (e.message == 500) {
        alert("server error!");
      }
    });
  resetform();
}

function getFormData() {
  var formData = {
    cid: parseInt(document.getElementById("cid").value),
    coursename: document.getElementById("cname").value,
  };
  return formData;
}

function showCourse(data){
    const course= JSON.parse(data);
    newRow(course);
}

function showCourses(data){
    const courses= JSON.parse(data);
    courses.forEach((c)=>{
        newRow(c);
    });
};

function newRow(course){
    var table=document.getElementById("myTable");
    var row=table.insertRow(table.length);
    var td=[];
    for(i=0; i<table.rows[0].cells.length; i++){
        td[i]= row.insertCell(i);
    }

    td[0].innerHTML= course.cid;
    td[1].innerHTML= course.coursename;
    td[2].innerHTML= '<input type="button" onclick="deleteCourse(this)" value="delete" id="button-1">';
    td[3].innerHTML= '<input type="button" onclick="updateCourse(this)" value="edit" id="button-2">';
}

function resetform(){
    document.getElementById("cid").value="";
    document.getElementById("cname").value="";
}

var selectedRow= null;
function updateCourse(r){
    selectedRow=r.parentElement.parentElement;
    document.getElementById("cid").value=selectedRow.cells[0].innerHTML;
    document.getElementById("cname").value=selectedRow.cells[1].innerHTML;

    var btn=document.getElementById("button-add");
    cid=selectedRow.cells[0].innerHTML;
    if(btn){
        btn.innerHTML="update";
        btn.setAttribute("onclick", "update(cid)");
    }
}

function update(cid){
    var newData=getFormData();
    fetch("/course/" + cid, {
        method: "PUT",
        body: JSON.stringify(newData),
        headers: {"content-type": "application/json; charset=UTF-8"},
    }).then((res)=>{
        if(res.ok){
            selectedRow.cells[0].innerHTML=newData.cid;
            selectedRow.cells[1].innerHTML=newData.coursename;
            var button=document.getElementById("button-add");
            button.innerHTML="Add";
            button.setAttribute("onclick", "addCourse()");
            selectedRow=null;

        }else{
            alert("Server: Update request error.");
        }
        resetform();
    });
}

function deleteCourse(r){
    if(confirm("Are you sure want to delete this?")){
        selectedRow= r.parentElement.parentElement;
        cid= selectedRow.cells[0].innerHTML;

        fetch("/course/" + cid, {
            method: "DELETE",
            headers: {"content-type": "application/json; charset=UTF-8"},
        });
        var rowIndex= selectedRow.rowIndex;
        if(rowIndex>0){
            document.getElementById("myTable").deleteRow(rowIndex);
        }
        selectedRow= null;
    }
}
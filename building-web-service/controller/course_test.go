package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddCourse(t *testing.T){
	assert:=assert.New(t)
	url:="http://localhost:8080/course"
	var jsonStr=[]byte(`{"cid":109, "coursename":"Academic Skills"}`)

	req,_:=http.NewRequest("POST",url,bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.AddCookie(&http.Cookie{Name:"my-cookie", Value: "my-value"})

	client:=http.Client{}
	resp, err:=client.Do(req)

	if err!=nil{
		panic(err)
	}
	defer resp.Body.Close()

	body,_:=io.ReadAll(resp.Body)
	assert.Equal(http.StatusCreated, resp.StatusCode)
	expResp:=`{"status":"course added successfully"}`
	assert.JSONEq(expResp, string(body))
}

func TestGetCourse(t *testing.T){
	assert:=assert.New(t)
	c:=http.Client{}
	r,_:=c.Get("http://localhost:8080/course/109")
	body,_:=io.ReadAll(r.Body)
	assert.Equal(http.StatusOK, r.StatusCode)
	expResp:=`{"cid":109, "coursename":"Academic Skills"}`
	assert.JSONEq(expResp, string(body))
}




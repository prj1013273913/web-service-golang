package controller

import (
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"time"
)

func Signup(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&admin); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	saveErr := admin.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	// no error
	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "admin added"})
}

// Decoding JSON data refers to the process of converting JSON (JavaScript Object Notation) formatted data into native data structures of a programming language, such as objects, arrays, strings, numbers, or booleans. In Go, decoding JSON involves converting JSON data into Go data types.

// Create a Decoder: In Go, you typically create a JSON decoder using the json.NewDecoder() function. This decoder reads from an input source, such as an HTTP request body or a file.
// Decode JSON: Once you have a decoder, you use its Decode() method to parse and decode the JSON data. This method takes an input source (e.g., an io.Reader) and a reference to a variable where the decoded data will be stored.
// Map JSON to Go Types: During decoding, the JSON data is mapped to corresponding Go types. For example, JSON objects are typically mapped to Go structs, JSON arrays to slices or arrays, JSON strings to Go strings, JSON numbers to Go numeric types, and JSON booleans to Go bool.
// Handle Errors: Decoding JSON can fail due to various reasons, such as invalid JSON syntax or mismatched data types. It's important to handle any errors that may occur during the decoding process.
// Use Decoded Data: Once the JSON data is successfully decoded, you can use the resulting Go data structures in your program as needed. This could involve storing the data in variables, passing it to functions, or performing further processing.
// In the context of the provided code snippet, decoding JSON data from the HTTP request body allows the program to extract the admin information sent by the client (e.g., via a signup form) and convert it into a Go model.Admin struct, which can then be used to create a new admin record in the database

func Login(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	getErr := admin.Get()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
		return
	}
	// Set cookie
	cookie := http.Cookie{
		Name:    "my-cookie",
		Value:   "my-value",
		Expires: time.Now().Add(30 * time.Minute),
		Secure:  true,
	}
	http.SetCookie(w, &cookie)
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "success"})
}

// The `Logout` function in Go deletes a cookie named "my-cookie" and responds with a JSON message
// indicating that the cookie has been deleted.
func Logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "my-cookie",
		Expires: time.Now(),
	})

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "cookie deleted"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	cookie, err := r.Cookie("my-cookie")
	if err != nil {
		if err == http.ErrNoCookie {
			httpResp.RespondWithError(w, http.StatusSeeOther, "cookie not found")
			return false
		}
		httpResp.RespondWithError(w, http.StatusInternalServerError, "internal server error")
		return false
	}

	if cookie.Value != "my-value" {
		httpResp.RespondWithError(w, http.StatusSeeOther, "cookie does not match")
		return false
	}
	return true
}

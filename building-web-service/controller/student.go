package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// func Home(w http.ResponseWriter, r *http.Request) {
// 	p := mux.Vars(r)
// 	course := p["course"]
// 	fmt.Println(p)
// 	_, err := w.Write([]byte("hello World.\n This course is " + course))
// 	if err != nil {
// 		fmt.Println("error:", err)
// 	}
// 	// fmt.Fprintf(w, "add student handler")
// }

// add a new student to the database
func AddStudent(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	var stud model.Student

	decoder := json.NewDecoder(r.Body) //initialize a new JSON decoder it takes an `io.Reader` as input

	if err := decoder.Decode(&stud); err != nil { //read the json encoded data from the input stream and store it in stud which is of type
		//model.Student which inturn is a struct type declared in the model package. ✌️


		// w.Write([]byte("Invalid json data"))
		// response, _ := json.Marshal(map[string]string{"error": "Invalid json body"})
		// w.Header().Set("content-Type", "application/json")
		// w.WriteHeader(http.StatusBadRequest)
		// w.Write(response)

		//updated using the response package from utils
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
 
	SaveErr := stud.Create()
	if SaveErr != nil {
		// w.Write([]byte("Database error"))
		// response, _ := json.Marshal(map[string]string{"error": SaveErr.Error()})
		// w.Header().Set("Content-Type", "application/json")
		// w.WriteHeader(http.StatusBadRequest)
		// w.Write(response)

		//updated using the response package from utils
		httpResp.RespondWithError(w, http.StatusBadRequest, SaveErr.Error())
		return
	}
	// w.Write([]byte("response success."))
	// fmt.Fprintf(w, "add student handler")

	// response, _ := json.Marshal(map[string]string{"status": "student added"})
	// w.Header().Set("Content-Type", "application/json")
	// w.WriteHeader(http.StatusCreated)
	// w.Write(response)

	//updated using the response package from utils
	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "student added"})
}

// Get the details of a student having the given ID.
func GetStud(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	sid := mux.Vars(r)["sid"]
	stdId, idErr := getUserId(sid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := model.Student{StdId: stdId}
	getErr := s.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, s)
}

func getUserId(userIdParam string) (int64, error) {
	userId, userErr := strconv.ParseInt(userIdParam, 10, 64)
	if userErr != nil {
		return 0, userErr
	}
	return userId, nil
}

// Update the detais of a student with a given ID
func UpdateStud(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	old_sid := mux.Vars(r)["sid"]
	old_stdId, idErr := getUserId(old_sid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	var stud model.Student
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&stud); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	updateErr := stud.Update(old_stdId)
	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Student not found")

		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, stud)
	}
}

// Delete the details of a student having the given id from the database.

func DeleteStud(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	sid := mux.Vars(r)["sid"]
	stdId, idErr := getUserId(sid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := model.Student{StdId: stdId}
	if err := s.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func GetAllStuds(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	students, getErr := model.GetAllStudents()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, students)
}

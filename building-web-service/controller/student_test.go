package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

// The commented out code block is a test function named `TestAddStudent` that is meant to test the
// functionality of adding a student to the server. Here's a breakdown of what the code is doing:
func TestAddStudent(t *testing.T){
	url:="http://localhost:8080/student" //Defines the URL of the endpoint where the student data will be sent✌️.
	var jsonStr=[]byte(`{"stdid":1004, "fname":"Sangay", "lname": "Lhamo", "email":"sl@gmail.com"}`) //Constructs a byte slice from a JSON 
	// string that will be sent to the server in the request body. It is essential to convert the JSON string to byte slice since data can 
	// only be transfered in the form of byte slice over the net/http package of GO 👍.
	req,_:=http.NewRequest("POST", url, bytes.NewBuffer(jsonStr)) //Creates a new HTTP POST request with the specified URL and request body containing the JSON data. 👍
	// By using bytes.NewBuffer(jsonStr), you're creating a bytes.Buffer instance that implements the io.Reader interface, allowing it to
	//be used as the request body in http.NewRequest(). This ensures that the JSON data is properly interpreted and sent as the request 
	// body.
	req.Header.Set("Content-Type", "application/json") // Sets the Content-Type header of the request to indicate that the request body contains JSON data ✌️.
	req.AddCookie(&http.Cookie{Name: "my-cookie", Value: "my-value"}) //Adds a cookie to the request. In this case, it's simulating sending a cookie along with the request.✌️

	client:=http.Client{} //creates an HTTP client that will be used to send the request to the server like a normal client.😒
	resp, err:=client.Do(req)// sends the HTTP request and retrieves the response.👌

	if err!=nil{
		panic(err)
	}

	defer resp.Body.Close()

	body,_:=io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	expResp:=`{"status":"student added"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestGetStudent(t *testing.T){
	c:=http.Client{}
	r,_:=c.Get("http://localhost:8080/student/1004")
	body,_:=io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)
	expResp:=`{"stdid":1004, "fname":"Sangay", "lname":"Lhamo", "email":"sl@gmail.com"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestDeleteStudent(t *testing.T){
	url:="http://localhost:8080/student/1004"
	req,_:=http.NewRequest("DELETE", url, nil)
	req.AddCookie(&http.Cookie{Name: "my-cookie", Value: "my-value"})
	client:=http.Client{}
	resp, err:=client.Do(req)
	if err !=nil{
		panic(err)
	}
	defer resp.Body.Close()

	body,_:=io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp:=`{"status":"deleted"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestStudentNotFound(t *testing.T){
	assert:=assert.New(t) //we can also use assert this way. These assertions help validate the expected behavior of your code and ensure that it behaves as intended.✌️
	c:=http.Client{}
	r,_:=c.Get("http://localhost:8080/student/1004")
	body,_:=io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp:=`{"error":"Student not found"}`
	assert.JSONEq(expResp, string(body)) //we don't need to reference the 't' here like in other tests.
}
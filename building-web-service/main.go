package main

import (
	"myapp/routes"
)

func main() {
	// router := mux.NewRouter()
	// router.HandleFunc("/home/{course}", homeHandler)
	// err := http.ListenAndServe(":8080", router)
	// if err != nil {
	// 	return
	// }
	routes.InitializeRoutes()
}

// func homeHandler(w http.ResponseWriter, r *http.Request) {
// 	p := mux.Vars(r)
// 	course := p["course"]
// 	fmt.Println(p)
// 	_, err := w.Write([]byte("hello World.\n This course is " + course))
// 	if err != nil {
// 		fmt.Println("error:", err)
// 	}
// }
